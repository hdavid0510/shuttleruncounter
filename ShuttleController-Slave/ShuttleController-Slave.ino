/*
 * PlaTec 2015 Copyright David(JeJoon).Hong
 * All rights reserved.
 * 
 * Code for Shuttle Run Timer.
*/

//다음 define 은 미사용시 주석처리바람
//#define DEBUG_sl       //디버그 모드. 셔틀런 카운터 시간 무시.
//#define DEBUG_led   //LED체인 오류 디버그. 남은 시간 표시 안함
//#define DEBUG_bp    //소리 디버그. 비프음 무시

// 사용 라이브러리 선언 ///////////////////////////////////////////////
#include <SoftwareSerial.h>
#include <SPI.h>
#include <WS2801.h>
//#include <SPLLib_WS2801.h>
// #include <LiquidCrystal_I2C.h>



// 작동 설정 //////////////////////////////////////////////////////
#define REFRESHTIME     100 // 새로고침시간(밀리초)
#define SENSORPIN       3   // IR센서가 꽂힌 아날로그핀(AI) 번호
#define SENSETHRSHLD    200 // IR센서 물체 감지 범위 (이 이상이 되면 감지)
#define SPEAKERPIN      9   // 스피커가 연결된 디지털핀(DIO) 번호
#define SPL_WS2801_Dpin 18  //변경금지! SPL두이노 LED체인(I2C) 신호연결선#1(데이터선)
#define SPL_WS2801_Cpin 19  //변경금지! SPL두이노 LED체인(I2C) 신호연결선#2(클럭선)
#define ON              255
#define ONlow           192
#define ONhalf          128
#define OFF             0



// 기기간 통신 위한 명령/정보 전달코드 ////////////////////////////////////
#define SigSlaveFinding	10
#define SigSlaveSensed	11
#define SigSlaveWait	20
#define SigSlaveFound   21
#define SigSlaveDoBeep	30
#define SigDoBeep       31
#define SigPrintCh      32
#define ResSlaveSensed	22
#define SigSlaveCounter	35
#define SigSlaveTime	36
#define SigSlaveDone	39
#define SigPhoneDoBeep	50
#define SigPhoneCounter	55
#define SigPhoneTime	56
#define SigPrintPosCntL 1
#define SigPrintPosCntR 2
#define SigPrintPosTmrL 3
#define SigPrintPosTmrR 4
#define pinSerialRX     2   //수신 핀
#define pinSerialTX     3   //송신 핀 *PWM지원 핀(3,5,6,9,10,11)이어야 함
SoftwareSerial slaveSerial(pinSerialRX,pinSerialTX); //Slave기기와 통신 가능하게 해주는 장치



// 셔틀런 규칙 및 결과 ///////////////////////////////////////////////
#define SUCCESS     1
#define FAIL        2
#define CNTLOST     3
const uint16_t shuttleruntime[9]
	={ 9000, 8000, 7500, 7000, 6700, 6500, 6200, 6000, 5700 };	//단계별 제한시간
const uint8_t shuttlerunthreshold[9]
	= { 7, 15, 23, 32, 41, 51, 61, 72, 83 };	//단계가 나뉘는 횟수



//LED위치 저장 ////////////////////////////////////////////////////
//#define ledPower    47			// 전원표시 LED
//#define ledSense	48			// 센서에 감지시 색 바뀌는 LED
//#define ledConnect  49			// Slave장치 연결상태 LED
//#define ledPoint	32			// 소수점
const uint8_t ledCntL[] = {15, 14, 13, 16, 17, 20, 19, 18, 21, 22, 25, 24, 23};
const uint8_t ledCntR[] = {12, 11, 10,  8,  9,  7,  6,  5,  3,  4,  2,  1,  0};
const uint8_t ledTmrL[] = {27, 28, 29, 30, 31};
const uint8_t ledTmrR[] = {43, 44, 45, 42, 41, 38, 39, 40, 37, 36, 33, 34, 35};
WS2801 ledChain = WS2801(50, SPL_WS2801_Dpin, SPL_WS2801_Cpin); // LED체인이름 설정



// 숫자 및 문자 배열 ////////////////////////////////////////////
const uint16_t numset[] = {
	 0x1FBF				// 0
	,0x0529				// 1
	,0x1DF7				// 2
	,0x1D6F				// 3
	,0x17E9				// 4
	,0x1ECE				// 5
	,0x12FF				// 6
	,0x1F29				// 7
	,0x1FFF				// 8
	,0x1FE9	};			// 9

#define chA 	0x0BFD
#define chB 	0x1BDE
#define chC 	0x1E97
#define chD 	0x1BBE
#define chE 	0x1ED7
#define chF 	0x1ED4
#define chG 	0x1EBF
#define chP 	0x1FF4
#define chR 	0x1BDD
#define chS 	0x1E4F
#define chn 	0x1FBD
#define chQmark	0x1D62
#define chEmark	0x0521
#define cht 	0x12D6




// FUNCTION ///////////////////////////////////////////////////////
void startdevice(){
	//startInfLed();
}

void fillLED(const uint8_t r, const uint8_t g, const uint8_t b) {
	for (uint8_t i = 0; i < ledChain.numPixels(); i++) {
		ledChain.setPixelColor(i, r, g, b);
	}
	ledChain.show();
}

//void startInfLed() {
//	ledChain.setPixelColor(ledPower, 255, 0, 0);
//	ledChain.setPixelColor(ledConnect, 255, 0, 0);
//	ledChain.setPixelColor(ledSense, 255, 0, 0);
//	ledChain.show();
//}

void printChar(const uint8_t *pos, const uint16_t ch, const uint8_t R =255, const uint8_t G = 255, const uint8_t B = 255){
	static uint8_t *prevpos;
    static uint16_t prevch;
    static uint8_t prevR, prevG, prevB;

    if(pos==prevpos && ch==prevch && R==prevR && G==prevG && B==prevB){
        Serial.println("printChar> ignored");
        return;
    }else{
        prevpos=(uint8_t *)pos;
        prevch=ch;
        prevR=R;
        prevG=G;
        prevB=B;
        uint16_t cha = ch;
        
        Serial.print("printChar> \""); Serial.print(cha,HEX); Serial.print("\"printing color:");
            Serial.print(R);Serial.print(" ");Serial.print(G);Serial.print(" ");Serial.println(B);
        
        for (short i = 12; i >= 0; i--){
            ledChain.setPixelColor(pos[i], (cha & 0x1)*R, (cha & 0x1)*G, (cha & 0x1)*B);
            cha >>= 1;
        }
        ledChain.show();
    }
}


// CALLBACK FUNCTIONS /////////////////////////////////////////////
void setup() {
    slaveSerial.begin(9600);
	Serial.begin(9600);
	ledChain.begin();
	startdevice();

    /******************* WAIT TO CONNECT ********************/
    printChar(ledCntL,chC,255,0,0); printChar(ledCntR,chQmark,255,0,0);
#ifndef DEBUG_sl
    Serial.println("SLAVE DEVICE: slave connection finding");
    while(!slaveSerial.available() || slaveSerial.read()!=SigSlaveFound){
        slaveSerial.write(SigSlaveFinding);
    }
    Serial.println("SLAVE DEVICE: CONNECTED!!");
    delay(500);
    Serial.println("SLAVE DEVICE: flushing buffer"); slaveSerial.flush();
    
#endif
    for(int i=0; i<50; i++) ledChain.setPixelColor(i,OFF,OFF,OFF);
    Serial.print("Ready");
    printChar(ledCntL, chG, OFF,ON,OFF); printChar(ledCntR, numset[0], OFF,ON,OFF);
    ledChain.show();
}

void loop(){
    byte sensedata=analogRead(SENSORPIN);
//    Serial.print("SENSOR: "); Serial.print(sensedata); Serial.print("\t");
	if(slaveSerial.available()){
        byte data = slaveSerial.read(); //0번째
        Serial.println(data);
        

        if(data==SigPrintCh){
            delay(20);
            byte pos= slaveSerial.read();//1
            uint16_t ch = slaveSerial.read();//2
            ch<<=8; ch+= slaveSerial.read();//3
            byte R=slaveSerial.read();
            byte G=slaveSerial.read();
            byte B=slaveSerial.read();//4,5,6
            switch(pos){
                case SigPrintPosCntL: printChar(ledCntL, ch, R,G,B); break;
                case SigPrintPosCntR: printChar(ledCntR, ch, R,G,B); break;
                case SigPrintPosTmrL: printChar(ledTmrL, ch, R,G,B); break;
                case SigPrintPosTmrR: printChar(ledTmrR, ch, R,G,B); break;
            }
            Serial.print("printdata :");Serial.print(ch);Serial.print("\t");
            Serial.print(R); Serial.print("\t");Serial.print(G); Serial.print("\t");Serial.println(B);
        }else
        if(data==SigDoBeep){
#ifndef DEBUG_bp
            tone(9,880,50); tone(10,440,50);
#else
            Serial.println("BEEP");
#endif
        }//else 
	}

   if(sensedata>= SENSETHRSHLD){
        slaveSerial.write(SigSlaveSensed);
        Serial.print("SENSED: ");Serial.println(sensedata);
   }
//   Serial.println("");
   delay(REFRESHTIME);
}
